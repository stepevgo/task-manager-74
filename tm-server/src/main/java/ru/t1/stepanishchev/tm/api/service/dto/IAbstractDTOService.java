package ru.t1.stepanishchev.tm.api.service.dto;

import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.dto.model.AbstractModelDTO;

@Service
public interface IAbstractDTOService<M extends AbstractModelDTO> {
}
