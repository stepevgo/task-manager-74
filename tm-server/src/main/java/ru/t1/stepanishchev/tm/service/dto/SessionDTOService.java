package ru.t1.stepanishchev.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.stepanishchev.tm.api.service.dto.ISessionDTOService;
import ru.t1.stepanishchev.tm.dto.model.SessionDTO;
import ru.t1.stepanishchev.tm.exception.entity.SessionNotFoundException;
import ru.t1.stepanishchev.tm.exception.field.IdEmptyException;
import ru.t1.stepanishchev.tm.exception.field.UserIdEmptyException;
import ru.t1.stepanishchev.tm.repository.dto.SessionDTORepository;

@Service
@NoArgsConstructor
public class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO>
        implements ISessionDTOService {

    @NotNull
    @Autowired
    public SessionDTORepository repository;

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public SessionDTO create(@Nullable final SessionDTO session) {
        if (session == null) throw new SessionNotFoundException();
        repository.save(session);
        return session;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public SessionDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO session = findOneById(id);
        if (session == null) throw new SessionNotFoundException();
        repository.delete(session);
        return session;
    }

}