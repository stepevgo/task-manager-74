package ru.t1.stepanishchev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.dto.model.ProjectDTO;

@Service
public interface IProjectTaskDTOService {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    ProjectDTO removeProjectById(@Nullable String userId, @NotNull String projectId);

}