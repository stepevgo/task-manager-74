package ru.t1.stepanishchev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.api.service.*;
import ru.t1.stepanishchev.tm.api.service.dto.*;
import ru.t1.stepanishchev.tm.endpoint.*;
import ru.t1.stepanishchev.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private IUserDTOService userService;

    @NotNull
    @Autowired
    private ISessionDTOService sessionService;

    @NotNull
    @Autowired
    private IAuthService authService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @SneakyThrows
    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    public void start() {
        loggerService.initJmsLogger();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        initPID();
        initEndpoints();
    }

    public void stop() {
        loggerService.info("** TASK MANAGER IS DONE **");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = String.format("http://%s:%s/%s?WSDL",
                propertyService.getServerHost(),
                propertyService.getServerPort(), name);
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

}