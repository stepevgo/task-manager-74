package ru.t1.stepanishchev.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.api.service.model.IUserOwnedService;
import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;
import ru.t1.stepanishchev.tm.repository.model.AbstractUserOwnedRepository;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel>
        extends AbstractService<M>
        implements IUserOwnedService<M> {

    @Nullable
    protected AbstractUserOwnedRepository<M> repository;

}