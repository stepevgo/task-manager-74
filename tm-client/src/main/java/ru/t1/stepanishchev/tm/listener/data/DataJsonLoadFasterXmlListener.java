package ru.t1.stepanishchev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.request.DataJsonLoadFasterXmlRequest;
import ru.t1.stepanishchev.tm.enumerated.Role;
import ru.t1.stepanishchev.tm.event.ConsoleEvent;

@Component
public final class DataJsonLoadFasterXmlListener extends AbstractDataListener {

    @NotNull
    private final static String NAME = "data-load-json";

    @NotNull
    private final static String DESCRIPTION = "Load data from json file.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonLoadFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA LOAD JSON]");
        domainEndpoint.loadDataJsonFasterXml(new DataJsonLoadFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{ Role.ADMIN };
    }

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

}