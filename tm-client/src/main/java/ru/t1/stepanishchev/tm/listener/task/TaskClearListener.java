package ru.t1.stepanishchev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.request.TaskClearRequest;
import ru.t1.stepanishchev.tm.event.ConsoleEvent;

@Component
public final class TaskClearListener extends AbstractTaskListener {

    @NotNull
    private final static String NAME = "task-clear";

    @NotNull
    private final static String DESCRIPTION = "Remove all tasks.";

    @Override
    @EventListener(condition = "@taskClearListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASKS CLEAR]");
        taskEndpointClient.clearTask(new TaskClearRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}