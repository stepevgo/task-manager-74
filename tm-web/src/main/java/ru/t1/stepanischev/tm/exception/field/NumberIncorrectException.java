package ru.t1.stepanischev.tm.exception.field;

import ru.t1.stepanischev.tm.exception.AbstractException;

public final class NumberIncorrectException extends AbstractException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(final String value, final Throwable cause) {
        super("Error! This value /" + value + "\" is incorrect...");
    }
}