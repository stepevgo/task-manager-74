package ru.t1.stepanischev.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("ru.t1.stepanischev.tm")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}