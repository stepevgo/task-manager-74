package ru.t1.stepanischev.tm.exception.user;

public class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

    public ExistsEmailException(String email) {
        super("Error! Email '" + email + "'already exists...");
    }

}