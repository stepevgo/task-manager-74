package ru.t1.stepanischev.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.stepanischev.tm.model.TaskDTO;

import java.util.List;

@Repository
public interface ITaskWebRepository extends JpaRepository<TaskDTO, String> {

    void deleteAllByUserId(String userId);

    @Nullable
    List<TaskDTO> findAllByUserId(String userId);

    void deleteByIdAndUserId(String id, String userId);

    @Nullable
    TaskDTO findByIdAndUserId(String id, String userId);

}