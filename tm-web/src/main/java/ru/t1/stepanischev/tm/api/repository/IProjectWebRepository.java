package ru.t1.stepanischev.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.stepanischev.tm.model.ProjectDTO;

import java.util.List;

@Repository
public interface IProjectWebRepository extends JpaRepository<ProjectDTO, String> {

    @Nullable
    List<ProjectDTO> findAllByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    @Nullable
    ProjectDTO findByUserIdAndId(String userId, String id);

    void deleteAllByUserId(String userId);

}
